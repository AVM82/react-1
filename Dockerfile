FROM node:12 as build
COPY package*.json ./
COPY public ./public
COPY src ./src
RUN npm install
RUN npm run build
FROM nginx
COPY --from=build build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]