import * as React from "react";
import {Loader} from "semantic-ui-react";


class ShowLoader extends React.Component {

    render() {
        return (
            <div className="chat">
                <div className="loader-container">
                    <Loader size={"massive"} active inline='centered'>
                        <span>Loading...</span>
                    </Loader>
                </div>
            </div>
        )
    }
}

export default ShowLoader
