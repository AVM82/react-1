import React from "react";
import '../css/SendMessage.css'

class SendMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            message: ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    render() {
        return (
            <div className='message-input-wrp'>
                <div className='message-input'>
                    <input
                        onChange={this.handleChange}
                        value={this.state.message}
                        placeholder="Type your message here"
                    />
                    <div className='send-button'>
                        <button onClick={this.handleSubmit} className="ui circular twitter icon button">
                            <i className="share icon"/>
                        </button>
                    </div>
                </div>
            </div>

        );
    }

    handleChange(event) {
        this.setState({
            message: event.target.value
        })
    }

    handleSubmit(event) {
        if (this.state.message === '') return
        event.preventDefault()
        this.props.sendMessage(this.state.message)
        this.setState({
            message: ''
        })
    }
}

export default SendMessage;