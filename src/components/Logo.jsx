import React from "react";

class Logo extends React.Component {

    render() {
        return (
            <img src="../logo.svg" alt='some LOGO'/>
        );
    }
}

export default Logo
