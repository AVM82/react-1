import React from "react";
import '../css/EditMessage.css'

class EditMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            message: this.props.data.message
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleConfirm = this.handleConfirm.bind(this);
    }

    handleChange(event) {
        const newMessage = {...this.state.message};
        newMessage.text = event.target.value;
        this.setState({message: newMessage});
    }

    handleConfirm() {
        this.props.confirmEdit(this.state.message);
    }

    render() {
        return (
            <div className='edit-message'>
                <input
                    onChange={this.handleChange}
                    value={this.state.message.text}
                />
                <div className='confirm-icon'>
                    <a onClick={this.handleConfirm} href="/#"><i className="check icon"/>Confirm</a>
                </div>
            </div>
        )
    }


}

export default EditMessage;
