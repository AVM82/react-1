import React from 'react';
import './css/Chat.css';
import ShowLoader from "./components/ShowLoader";
import Logo from "./components/Logo";
import Header from "./components/Header";
import OtherMessage from "./components/OtherMessage";
import SendMessage from "./components/SendMessage";
import MyMessage from "./components/MyMessage";


class Chat extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            uniqueUser: undefined,
            messages: undefined,
            lastMessage: undefined,
            myID: 333,
        };

        this.sendMessage = this.sendMessage.bind(this);
        this.deleteMessage = this.deleteMessage.bind(this);
        this.updateMessage = this.updateMessage.bind(this);

    }

    create_UUID() {
        let dt = new Date().getTime();
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (dt + Math.random() * 16) % 16 | 0;
            dt = Math.floor(dt / 16);
            return (c === 'x' ? r : (r && 0x3 | 0x8)).toString(16);
        });
    }


    componentDidMount() {
        this.fetchMessage();
    }


    fetchMessage = () => {
        fetch('https://edikdolynskyi.github.io/react_sources/messages.json')
            .then((res) => res.json())
            .then((res) => {
                return res.sort((a, b) => {
                    const first = new Date(a.createdAt).getTime();
                    const second = new Date(b.createdAt).getTime();
                    if (first > second) {
                        return 1;
                    } else if (first < second) {
                        return -1;
                    }
                    return 0;
                })
            })
            .then((res) => {
                const count = Object.keys(res).length;
                this.setState({
                    messages: res,
                    loading: false,
                    uniqueUser: this.getUniqueUser(res),
                    lastMessage: this.convertDate(new Date(res[count - 1].createdAt).getTime())
                });

            })
            .catch((error) => {
                console.log(error);
            });
    };


    convertToOnlyDate(date) {
        return new Intl.DateTimeFormat("en-GB", {
            year: "numeric",
            month: "long",
            day: "2-digit",
        }).format(date);
    }

    convertDate(date) {
        return new Intl.DateTimeFormat("en-GB", {
            year: "numeric",
            month: "long",
            day: "2-digit",
            hour: "2-digit",
            minute: "2-digit",
            second: "2-digit"
        }).format(date);
    }


    getUniqueUser(res) {
        return res.reduce(function (a, d) {
            if (a.indexOf(d.userId) === -1) {
                a.push(d.userId);
            }
            return a;
        }, []);
    }

    sendMessage(text) {
        const date = new Date();
        this.setState(prevState => ({
            messages: [...prevState.messages, {
                id: this.create_UUID(),
                userId: 333,
                avatar: "",
                user: "Me",
                "text": text,
                "createdAt": date,
                "editedAt": ""
            }]
        }))
        this.setState({
            lastMessage: this.convertDate(date)
        })
        this.scrollToBottom();
    }

    scrollToBottom = () => {
        this.messagesEnd.scrollIntoView({behavior: "smooth"});
    }

    deleteMessage(uuid) {
        const array = [...this.state.messages];
        //thx https://gist.github.com/scottopolis/6e35cf0d53bae81e6161662e6374da04
        const removeIndex = array.map(function (item) {
            return item.id;
        }).indexOf(uuid);
        array.splice(removeIndex, 1);
        this.setState({
            messages: array
        })
    }

    updateMessage(message) {
        const array = [...this.state.messages];
        const updateIndex = array.map(function (item) {
            return item.id;
        }).indexOf(message.id);
        array[updateIndex] = message;
        this.setState({
            messages: array
        })
    }

    render() {
        if (this.state.loading) return <ShowLoader/>;
        let messageDate = undefined;
        return (

            <>
                <Logo/>
                <div className='chat-wrp'>
                    <Header data={
                        {
                            uniqueUser: this.state.uniqueUser.length + 1,
                            countMes: this.state.messages.length,
                            lastMessage: this.state.lastMessage
                        }
                    }/>
                    <div className='main-wrp'>
                        <div className='message-list-wrp'>
                            {
                                this.state.messages.map((item, index) => {
                                    let divider;
                                    let currentMesDate = this.convertToOnlyDate(new Date(item.createdAt).getTime())
                                    if (messageDate !== currentMesDate) {
                                        messageDate = currentMesDate;
                                        divider = <div className="ui horizontal divider">{currentMesDate}</div>
                                    } else {
                                        divider = null;
                                    }
                                    return (item.userId === 333) ?
                                        <MyMessage data={{message: item, divider: divider}}
                                                   deleteMessage={this.deleteMessage}
                                                   updateMessage={this.updateMessage}
                                                   key={index}/> :
                                        <OtherMessage data={{message: item, divider: divider}} key={index}/>
                                })
                            }
                            <div className='hack-padding'
                                 ref={(el) => {
                                     this.messagesEnd = el;
                                 }}>
                            </div>
                        </div>
                    </div>
                    <SendMessage sendMessage={this.sendMessage}/>
                </div>
            </>
        )
    }
}

export default Chat;
